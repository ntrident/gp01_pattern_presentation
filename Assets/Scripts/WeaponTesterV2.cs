﻿using UnityEngine;
using System.Collections;

public class WeaponTesterV2 : MonoBehaviour
{
    [SerializeField]
    private WeaponV2 currentWeapon;

    [SerializeField]
    private Target target;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            currentWeapon.Attack(target);
        }
    }
}
