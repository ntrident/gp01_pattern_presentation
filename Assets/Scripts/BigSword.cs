﻿using UnityEngine;
using System.Collections;

public class BigSword : Weapon
{
    protected override void DoAttack(Target target)
    {
        target.TakeDamage(15);
    }

    protected override string DamageMessage()
    {
        return "devastate";
    }
}
