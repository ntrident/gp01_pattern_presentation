﻿using UnityEngine;
using System.Collections;

public class WeaponV2 : MonoBehaviour
{
    [SerializeField]
    private WeaponData weaponData;

    public void Attack(Target target)
    {
        if (weaponData.Damage > 0)
            target.TakeDamage(weaponData.Damage);

        string message = string.IsNullOrEmpty(weaponData.Message) ? "hit" : weaponData.Message;
        Debug.Log("You " + message + " " + target.name);
    }
}
